#pragma once

#include <unistd.h>					// read
#include <string>

template <int bufsize = 128>		// buffer size as a compile-time parameter with default value
class file_descriptor_reader
{
public:
	file_descriptor_reader (int);	// constructor: binds the object to particular file descriptor
	file_descriptor_reader (file_descriptor_reader const&) = delete;	// disallow copying and copy-assignment
	file_descriptor_reader& operator= (file_descriptor_reader const&) = delete;
	std::string get_line ();
	char get_char ();
	bool is_open ();				// checks if the descriptor is still open
private:
	int file_d;						// descriptor
	char buf[bufsize] { 0 };		// reading buffer
	char * character { buf };		// actual position in the buffer
	int remaining { 0 };			// remaining unread characters in a buffer
	bool descr_open { false };		// open/close descriptor flag
	void close_descriptor ();		// close the descriptor
};

// ======= FILE_DESCRIPTOR_READER ======= //
template<int bufsize> file_descriptor_reader<bufsize>::file_descriptor_reader (int f)
	: file_d (f), descr_open (true)
	{}

// ======= GET_LINE ======= //
template<int bufsize> std::string file_descriptor_reader<bufsize>::get_line ()
{
	std::string s;
	while (true) {
		if (remaining == 0) {		// buffer is empty: fill it and put the reading character at the start
			remaining = read (file_d, buf, bufsize);
			character = buf;
			if (remaining <= 0) {	// error or empty descriptor: return what we've read and close
				close_descriptor ();
				return s;
			}
		}
		if (*character == '\n') {	// newline character: strip the newline char and return
			++character;
			--remaining;
			return s;
		} else {					// else append the character and continue
			s += *character++;
			--remaining;
		}
	}
}

// ======= GET_CHAR ======= //
template<int bufsize> char file_descriptor_reader<bufsize>::get_char ()
{
	if (remaining == 0) {		// buffer is empty: fill it and put the reading character at the start
		remaining = read (file_d, buf, bufsize);
		character = buf;
		if (remaining <= 0) {	// error or empty descriptor: return empty char and close
			close_descriptor ();
			return ' ';
		}
	}
	--remaining;
	return *character++;
}

// ======= IS_OPEN ======= //
template<int bufsize> bool file_descriptor_reader<bufsize>::is_open ()
{
	return descr_open;
}

// ======= CLOSE_DESCRIPTOR ======= //
template<int bufsize> void file_descriptor_reader<bufsize>::close_descriptor ()
{
	close (file_d);
	descr_open = false;
	return;
}


#include "shared_mem.h"
#include <cstdio>			// fprintf
#include <cstdlib>			// exit
#include <sys/ipc.h>		// ftok
#include <sys/shm.h>		// shmget, shmat, shmctl
#include <errno.h>			// errno

// ======= GET_SHARED_SPACE ======= //
shared_mem get_shared_space (int sz, program_options const& global_opts)
{
	shared_mem shm;
	
	key_t key = ftok (".", 1234);
	if (key == -1) {
		fprintf (stderr, "==SERVER ERROR: Key creation failed\n");
		exit (EXIT_FAILURE);
	}
	shm.shmid = shmget (key, sz, IPC_CREAT | 0666);
	if (shm.shmid == -1) {
		fprintf (stderr, "==SERVER ERROR: Shared memory creation failed, %d\n", errno);
		exit (EXIT_FAILURE);
	}
	shm.shmat = shmat (shm.shmid, NULL, 0);
	if (shm.shmat == (void *) -1) {
		fprintf (stderr, "==SERVER ERROR: Shared memory attachement failed\n");
		shmctl (shm.shmid, IPC_RMID, NULL);
		exit (EXIT_FAILURE);
	}
	return shm;
}

// ======= CLEAN_SHARED_SPACE ======= //
void clean_shared_space (shared_mem const& shm)
{
	fprintf (stderr, "==DESTRUCTOR: clean_shared_space\n");
	if (shmdt (shm.shmat) != 0) {
		fprintf (stderr, "==DESTRUCTOR: shmdt error\n");
		exit (EXIT_FAILURE);
	}
	if (shmctl (shm.shmid, IPC_RMID, NULL) == -1) {
		fprintf (stderr, "==DESTRUCTOR: shmctl error\n");
		exit (EXIT_FAILURE);
	}
	fprintf (stderr, "==SERVER: Shared memory cleaned\n");
	return;
}

// ======= DETACH ======= //
void detach (void * p)
{
	shmdt (p);
	return;
}

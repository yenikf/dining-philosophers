
#include "dining_table.h"
#include "utils.h"

// ======= DINING_TABLE ======= //
dining_table::dining_table (int number_seats)
{
	sz = number_seats;
	info ("==TABLE: Constructing with %d seats\n", sz);
	sem_init (&mut, 1, 1);
	sem_init (&empty_seats, 1, sz);
	for (int x = 0; x < sz; ++x) {
		sem_init (&s[x], 1, 0);
		stat[x] = empty;
	}
}

// ======= ~DINING_TABLE ======= //
dining_table::~dining_table ()
{
	sem_destroy (&mut);
	sem_destroy (&empty_seats);
	for (int x = 0; x < sz; ++x) {
		sem_destroy (&s[x]);
	}
}

// ======= GET_SEAT ======= //
int dining_table::get_seat (int p)
{
    int id = sz;
    while (id == sz) {
        sem_wait (&mut);
        id = find_empty ();
        if (id != sz) {
            stat[id] = thinking;
        }
        sem_post (&mut);
        sem_wait (&empty_seats);

		int i = 0;
		sem_getvalue (&empty_seats, &i);
		info ("==TABLE %d: get_seat(%d): empty_seats: %d\n", p, id, i);
	}
    return id;
}

// ======= TAKE_FORKS ======= //
void dining_table::take_forks (int id, int p)
{
	sem_wait (&mut);
	stat[id] = hungry;
	test (id, p);
	sem_post (&mut);
	sem_wait (&s[id]);
	info ("==TABLE %d: take_forks(%d)\n", p, id);
	return;
}

// ======= DROP_FORKS ======= //
void dining_table::drop_forks (int id, int p)
{
	sem_wait (&mut);
	stat[id] = thinking;
	test (left (id), p);
	test (right (id), p);
	info ("==TABLE %d: drop_forks(%d)\n", p, id);
	sem_post (&mut);
	return;
}

// ======= LEAVE ======= //
void dining_table::leave (int id, int p)
{
    sem_wait (&mut);
    stat[id] = empty;
    sem_post (&empty_seats);
	int i = 0;
	sem_getvalue (&empty_seats, &i);
	info ("==TABLE %d: leave(%d): empty_seats: %d\n", p, id, i);
    sem_post (&mut);
    return;
}

// ======= LEFT ======= //
int dining_table::left (int id)
{
	return (id + sz - 1) % sz;
}

// ======= RIGHT ======= //
int dining_table::right (int id)
{
	return (id + 1) % sz;
}

// ======= TEST ======= //
void dining_table::test (int id, int p)
{
	info ("==TABLE TEST %d: id: %d | left: %d | right: %d\n", p, stat[id], stat[left (id)], stat[right (id)]); 
	if ((stat[id] == hungry) && (stat[left (id)] != eating) && (stat[right (id)] != eating)) {
		info ("==TABLE TEST %d: passed\n", p);
		stat[id] = eating;
		sem_post (&s[id]);
	}
	return;
}

// ======= FIND_EMPTY ======= //
int dining_table::find_empty ()
{
	for (int i = 0; i < sz; ++i) {
		if (stat[i] == empty) {
			info ("==TABLE: found empty: %d\n", i);
			return i;
		}
	}
	return sz;
}

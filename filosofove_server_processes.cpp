/*
	Jan Feific | FEI0017 | 26.05.2019 - 02.06.2019
	OSY 2018/19 | tut6 - Projekt 2
	
	Solving the dining philosophers problem, Andrew Tanenbaum style
	using processes
 */

#include <cstdio>			// fprintf
#include <cstdlib>			// exit
#include <cstring>			// memset, strlen
#include <unistd.h>			// fork, read, write, close, sleep
#include <arpa/inet.h>		// sockaddr_in, inet_ntoa
#include <sys/socket.h>		// socket, bind, listen, accept, setsockopt, getpeername
#include <sys/select.h>		// select
#include <signal.h>			// signal
#include <string>			// string, stoi
#include "utils.h"
#include "file_descriptor_reader.hpp"
#include "shared_mem.h"
#include "dining_table.h"

// commandline options glob object:
// default number of seats, default port number, verbose by default, show help by default
program_options prg_opts = { 5, 25000, false, false };

// ======= PARSE_MESSAGE ======= //
int parse_message (std::string const& msg)
{
	int i = 0;								// command numcode default value
	if (msg[0] == 'C') {					// get command numcode
		i = std::stoi (&msg[1], nullptr, 10);
	}
	//fprintf (stderr, "==PARSED MESSAGE: %c|%d|%s\n", msg[0], i, msg.substr (4, msg.size ()).c_str ());
	return i;
}

// ======= SEND_MESSAGE ======= //
void send_message (int const descriptor, char const cmd_code, int const cmd_numcode, char const * msg)
{
	char ch[256] = { 0 };
	snprintf (ch, 256, "%c%02d:%s\n", cmd_code, cmd_numcode, msg);
	info ("==SERVER SENDING: %s", ch);
	write (descriptor, ch, strlen (ch));
	return;
}

// ======= SEND_MESSAGE ======= //
void send_message (int const descriptor, char const cmd_code, int const cmd_numcode, char const * msg, int place)
{
	char ch[256] = { 0 };
	snprintf (ch, 256, "%c%02d:%s %d\n", cmd_code, cmd_numcode, msg, place);
	info ("==SERVER SENDING: %s", ch);
	write (descriptor, ch, strlen (ch));
	return;
}

// ======= CHILD_PROCESS ======= //
void child_process (dining_table * tab, int const client_socket)
{
	sockaddr_in peer;						// get client's details
	socklen_t peer_sz = sizeof (peer);
	getpeername (client_socket, (sockaddr *) &peer, &peer_sz);
	fprintf (stderr, "==SERVER: Accepted connection from %s:%d.\n", inet_ntoa (peer.sin_addr), ntohs (peer.sin_port));

	file_descriptor_reader<> client_msg (client_socket);
	int seat;								// place by the table
	while (true) {
		std::string msg = client_msg.get_line ();
		if (msg.empty ()) {					// close client, clean after
			info ("==SERVER %d: Client ends\n", ntohs (peer.sin_port));
			tab->drop_forks (seat, ntohs (peer.sin_port));
			tab->leave (seat, ntohs (peer.sin_port));
			break;
		}
		info ("==MESSAGE FROM %s:%d: \"%s\"\n", inet_ntoa (peer.sin_addr), ntohs (peer.sin_port), msg.c_str ());
		switch (parse_message (msg)) {
			case 11:						// "Chci si sednout"
				seat = tab->get_seat (ntohs (peer.sin_port));
				send_message (client_socket, 'A', 12, "Sedis na zidli", seat);
				break;
			case 22:						// "Chci jist"
				tab->take_forks (seat, ntohs (peer.sin_port));
				send_message (client_socket, 'A', 23, "Muzes jist");
				break;
			case 33:						// "Dojedl jsem"
				tab->drop_forks (seat, ntohs (peer.sin_port));
				send_message (client_socket, 'A', 34, "Vidlicky polozeny");
				break;
			case 44:						// "Odchazim"
				tab->leave (seat, ntohs (peer.sin_port));
				send_message (client_socket, 'A', 45, "Nashledanou");
				return;
			default:						// not understood or not a command: drop the message
				break;
		}
	}
	return;
}

// ======= MAIN ======= //
int main (int argc, char ** argv)
{
	signal (SIGCHLD, SIG_IGN);						// ignore child processes' return values

	set_program_options (argc, argv, prg_opts);
	info ("==SERVER: global options: seats: %d, port number: %d, verbose: %d, help: %d\n",
		  prg_opts.table_seats, prg_opts.port, prg_opts.verbose, prg_opts.help);
	if (prg_opts.help == true) {					// print help and exit
		show_help ();
		exit (EXIT_SUCCESS);
	}

	// create table class in a shared memory segment
	shared_mem shm = get_shared_space (sizeof (dining_table), prg_opts);
	info ("==SERVER: shared space with the size of %d bytes created\n", sizeof (dining_table)); 
	dining_table * table = new (shm.shmat) dining_table (prg_opts.table_seats);

	int server_socket = socket (AF_INET, SOCK_STREAM, 0);	// create a server socket
	if (server_socket == -1) {
		fprintf (stderr, "==SERVER ERROR: Socket creation failed\n");
		exit (EXIT_FAILURE);
	}

	/*
	 * SOCKADDR_IN  sockaddr parallel structure for use with IPv4
	 * struct sockaddr_in {
	 *   short int       sin_family;   Address family - AF_INET
	 *   unsigned short  sin_port;     Port number
	 *   struct in_addr  sin_addr;     Internet address
	 *   unsigned char   sin_zero[8];  So this struct is the same size as sockaddr - should be zeroed out
	 * }
	 * 
	 * IN_ADDR  internet address structure, IPv4 only
	 * struct in_addr {
	 *   uint32_t        s_addr;       four bytes IPv4 address
	 * }
	 */
	sockaddr_in address;							// configure the address format
	address.sin_family = AF_INET;					// IPv4 connection
	address.sin_port = htons (prg_opts.port);		// host-to-network-short
	address.sin_addr.s_addr = INADDR_ANY;			// any address
	memset (address.sin_zero, 0, 8);				// zero out the padding space

	int reuse = 1;									// set port for reuse
	if (setsockopt (server_socket, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof (reuse)) < 0) {
		fprintf (stderr, "==SERVER ERROR: Setting address for reuse failed\n");
		close (server_socket);
		exit (EXIT_FAILURE);
	}

	if (bind (server_socket, (sockaddr *) &address, sizeof (address)) < 0) {
		fprintf (stderr, "==SERVER ERROR: Binding socket failed\n");
		close (server_socket);
		exit (EXIT_FAILURE);
	}

	if (listen (server_socket, 64) < 0) {
		fprintf (stderr, "==SERVER ERROR: Listening failed\n");
		close (server_socket);
		exit (EXIT_FAILURE);
	}

	info ("==SERVER: Ready to accept on %s:%d.\n", inet_ntoa (address.sin_addr), prg_opts.port);

	sockaddr_in client_addr;						// client socket data structure
	socklen_t client_len = sizeof (client_addr);
	while (true) {
		fd_set filedesc_set;
		FD_ZERO (&filedesc_set);
		FD_SET (STDIN_FILENO, &filedesc_set);
		FD_SET (server_socket, &filedesc_set);

		int ret = select (server_socket + 1, &filedesc_set, 0, 0, 0);
		if (ret <= 0) {								// select error
			fprintf (stderr, "==SERVER ERROR: Select failed\n");
			close (server_socket);
			break;
		} else {
			if (FD_ISSET (STDIN_FILENO, &filedesc_set)) {	// something on stdin
				char ch[10] = { 0 };
				read (STDIN_FILENO, ch, 10);
				info ("SERVER: STDIN input: %s\n", ch);
				if (ch[0] == 'q') {
					info ("SERVER: STDIN quit\n", ch);
					break;
				}
			}
			if (FD_ISSET (server_socket, &filedesc_set)) {	// something to accept
				int client_socket = accept (server_socket, (sockaddr *) &client_addr, &client_len);
				if (client_socket < 0) {
					fprintf (stderr, "==SERVER ERROR: Accepting connection failed\n");
					close (server_socket);
					close (client_socket);
					exit (EXIT_FAILURE);
				}

				pid_t pid = fork ();
				if (pid == -1) {					// forking error
					fprintf (stderr, "==SERVER ERROR: Forking process failed\n");
					close (server_socket);
					close (client_socket);
					exit (EXIT_FAILURE);
				} else if (pid == 0) {				// child process: handle request
					close (server_socket);			// child doesn't need this

					child_process (table, client_socket);

					detach (shm.shmat);				// detach from shared memory
					close (client_socket);			// close and exit child process
					exit (EXIT_SUCCESS);
				}

				// parent process
				close (client_socket);				// parent doesn't need this
			}
		}
	}

	close (server_socket);
	table->~dining_table ();
	clean_shared_space (shm);
	return 0;
}

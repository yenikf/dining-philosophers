
#include "utils.h"
#include <cstdlib>			// atoi
#include <cstdio>			// fprintf
#include <cstdarg>			// vfprintf, va_list, va_start, va_end
#include <unistd.h>			// getopt

extern program_options prg_opts;

// ======= SET_PROGRAM_OPTIONS ======= //
void set_program_options (int argc, char ** argv, program_options& options)
{
	for (int opt; (opt = getopt (argc, argv, "s:p:vh")) != -1; ) {
		switch (opt) {
			case 's':
				if (0 < atoi (optarg) && atoi (optarg) <= 21) {
					options.table_seats = atoi (optarg);
				} else {
					fprintf (stderr, "invalid value of parameter -s: %d, using the default (%d)\n",
							 atoi (optarg), prg_opts.table_seats);
				}
				break;
			case 'p':
				if (1023 < atoi (optarg) && atoi (optarg) <= 65535) {
					options.port = atoi (optarg);
				} else {
					fprintf (stderr, "invalid value of parameter -p: %d, using the default (%d)\n",
							 atoi (optarg), prg_opts.port);
				}
				break;
			case 'v':
				options.verbose = true;
				break;
			case 'h':
				options.help = true;
				break;
			default:
				break;
		}
	}
	return;
}

// ======= SHOW_HELP ======= //
void show_help ()
{
	fprintf (stdout,
		"filosofove : Solving the dining philosophers problem!\n\n"
		"Usage: filosofove [OPTIONS]\n"
		"Options:\n"
		" -s SEATS      number of seats by the table (default 5, max 21)\n"
		" -p PORTNUM    port number (default 25000)\n"
		" -v            verbose output flag (default false)\n"
		" -h            print this help\n\n"
		" Type 'q' to quit the program\n");
	return;
}

// ======= INFO ======= //
void info (char const * msg, ...)
{
	if (prg_opts.verbose == true) {
		va_list arglist;
		va_start (arglist, msg);
		vfprintf (stderr, msg, arglist);
		va_end (arglist);
	}
	return;
}

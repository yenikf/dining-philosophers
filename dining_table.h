#pragma once

#include <semaphore.h>		// sem_t, sem_init, sem_destroy, sem_wait, sem_post

class dining_table
{
public:
	dining_table (int);
	~dining_table ();
	int get_seat (int);					// take place by the table
	void take_forks (int, int);			// got hungry, want to grab resources
	void drop_forks (int, int);			// finished eating, drop resources
	void leave (int, int);				// leave the table
private:
	int sz;								// dining table size, default is 5
	int left (int);						// left neighbour
	int right (int);					// right neighbour
	void test (int, int);
	int find_empty ();					// find empty seat
	enum state { empty, thinking, hungry, eating };		// possible states of seats
	sem_t mut;							// whole-class mutex
	sem_t empty_seats;					// semaphore counting the empty seats, equals sz
	sem_t s[21];						// seats
	state stat[21];						// states of seats
};

#pragma once

// command line options of the whole program
struct program_options
{
	int table_seats;					// number of seats
	int port;							// port number
	bool verbose;						// verbose output
	bool help;							// show help instead
};

void set_program_options (int, char **, program_options&);
void show_help ();
void info (char const *, ...);			// verbose output


#include "utils.h"			// program_options

// shared memory info structure
struct shared_mem
{
	int shmid;				// shared memory ID
	void * shmat;			// shared memory pointer
};

shared_mem get_shared_space (int sz, program_options const&);
void clean_shared_space (shared_mem const&);
void detach (void *);		// detach but don't clean (child processes)

/*
	Jan Feific | FEI0017 | 02.06.2019 - 03.06.2019
	OSY 2018/19 | tut6 - Projekt 2
	
	Solving the dining philosophers problem, Andrew Tanenbaum style
	using threads
 */

#include <cstdio>			// fprintf
#include <cstdlib>			// exit
#include <cstring>			// memset, strlen
#include <unistd.h>			// read, write, close, sleep
#include <arpa/inet.h>		// sockaddr_in, inet_ntoa
#include <sys/socket.h>		// socket, bind, listen, accept, setsockopt, getpeername
#include <sys/select.h>		// select
#include <signal.h>			// signal
#include <pthread.h>		// pthread_create, pthread_detach
#include <ctime>			// usleep
#include <string>			// string, stoi
#include "utils.h"
#include "file_descriptor_reader.hpp"
#include "dining_table.h"

struct pthread_arg_struct {					// data passed to the thread
	dining_table * table;
	int socket;
};

sem_t thr_mut_in;							// thread initialization semaphore - parent sending to child
sem_t thr_mut_out;							// child signalling the parent

// commandline options glob object:
// default number of seats, default port number, verbose by default, show help by default
program_options prg_opts = { 5, 25000, false, false };

// ======= PARSE_MESSAGE ======= //
int parse_message (std::string const& msg)
{
	int i = 0;								// command numcode default value
	if (msg[0] == 'C') {					// get command numcode
		i = std::stoi (&msg[1], nullptr, 10);
	}
	fprintf (stderr, "==PARSED MESSAGE: %c|%d|%s\n", msg[0], i, msg.substr (4, msg.size ()).c_str ());
	return i;
}

// ======= SEND_MESSAGE ======= //
void send_message (int const descriptor, char const cmd_code, int const cmd_numcode, char const * msg)
{
	char ch[256] = { 0 };
	snprintf (ch, 256, "%c%02d:%s\n", cmd_code, cmd_numcode, msg);
	info ("==SERVER SENDING: %s", ch);
	write (descriptor, ch, strlen (ch));
	return;
}

// ======= SEND_MESSAGE ======= //
void send_message (int const descriptor, char const cmd_code, int const cmd_numcode, char const * msg, int place)
{
	char ch[256] = { 0 };
	snprintf (ch, 256, "%c%02d:%s %d\n", cmd_code, cmd_numcode, msg, place);
	info ("==SERVER SENDING: %s", ch);
	write (descriptor, ch, strlen (ch));
	return;
}

// ======= CHILD_PROCESS ======= //
void * child_process (void * params)
{
	pthread_arg_struct parameters = *((pthread_arg_struct *) params);
	dining_table * tab = parameters.table;
	int client_socket = parameters.socket;
	sockaddr_in peer;						// get client's details
	socklen_t peer_sz = sizeof (peer);
	getpeername (client_socket, (sockaddr *) &peer, &peer_sz);
	fprintf (stderr, "==SERVER: Accepted connection from %s:%d.\n", inet_ntoa (peer.sin_addr), ntohs (peer.sin_port));

	sem_post (&thr_mut_out);				// notify the parent
	sem_post (&thr_mut_in);					// data are copied

	file_descriptor_reader<> client_msg (client_socket);
	int seat;								// place by the table
	while (true) {
		std::string msg = client_msg.get_line ();
		if (msg.empty ()) {					// close client, clean after
			info ("==SERVER %d: Client ends\n", ntohs (peer.sin_port));
			tab->drop_forks (seat, ntohs (peer.sin_port));
			tab->leave (seat, ntohs (peer.sin_port));
			break;
		}
		info ("==MESSAGE FROM %s:%d: \"%s\"\n", inet_ntoa (peer.sin_addr), ntohs (peer.sin_port), msg.c_str ());
		switch (parse_message (msg)) {
			case 11:						// "Chci si sednout"
				seat = tab->get_seat (ntohs (peer.sin_port));
				send_message (client_socket, 'A', 12, "Sedis na zidli", seat);
				break;
			case 22:						// "Chci jist"
				tab->take_forks (seat, ntohs (peer.sin_port));
				send_message (client_socket, 'A', 23, "Muzes jist");
				break;
			case 33:						// "Dojedl jsem"
				tab->drop_forks (seat, ntohs (peer.sin_port));
				send_message (client_socket, 'A', 34, "Vidlicky polozeny");
				break;
			case 44:						// "Odchazim"
				tab->leave (seat, ntohs (peer.sin_port));
				send_message (client_socket, 'A', 45, "Nashledanou");
				close (client_socket);
				pthread_exit (NULL);
			default:						// not understood or not a command: drop the message
				break;
		}
	}
	close (client_socket);
	pthread_exit (NULL);
}

// ======= MAIN ======= //
int main (int argc, char ** argv)
{
	signal (SIGCHLD, SIG_IGN);						// ignore child processes' return values

	set_program_options (argc, argv, prg_opts);
	info ("==SERVER: global options: seats: %d, port number: %d, verbose: %d, help: %d\n",
		  prg_opts.table_seats, prg_opts.port, prg_opts.verbose, prg_opts.help);
	if (prg_opts.help == true) {					// print help and exit
		show_help ();
		exit (EXIT_SUCCESS);
	}
	
	sem_init (&thr_mut_in, 1, 1);
	sem_init (&thr_mut_out, 1, 0);

	dining_table table (prg_opts.table_seats);

	int server_socket = socket (AF_INET, SOCK_STREAM, 0);	// create a server socket
	if (server_socket == -1) {
		fprintf (stderr, "==SERVER ERROR: Socket creation failed\n");
		exit (EXIT_FAILURE);
	}

	/*
	 * SOCKADDR_IN  sockaddr parallel structure for use with IPv4
	 * struct sockaddr_in {
	 *   short int       sin_family;   Address family - AF_INET
	 *   unsigned short  sin_port;     Port number
	 *   struct in_addr  sin_addr;     Internet address
	 *   unsigned char   sin_zero[8];  So this struct is the same size as sockaddr - should be zeroed out
	 * }
	 * 
	 * IN_ADDR  internet address structure, IPv4 only
	 * struct in_addr {
	 *   uint32_t        s_addr;       four bytes IPv4 address
	 * }
	 */
	sockaddr_in address;							// configure the address format
	address.sin_family = AF_INET;					// IPv4 connection
	address.sin_port = htons (prg_opts.port);		// host-to-network-short
	address.sin_addr.s_addr = INADDR_ANY;			// any address
	memset (address.sin_zero, 0, 8);				// zero out the padding space

	int reuse = 1;									// set port for reuse
	if (setsockopt (server_socket, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof (reuse)) < 0) {
		fprintf (stderr, "==SERVER ERROR: Setting address for reuse failed\n");
		close (server_socket);
		exit (EXIT_FAILURE);
	}

	if (bind (server_socket, (sockaddr *) &address, sizeof (address)) < 0) {
		fprintf (stderr, "==SERVER ERROR: Binding socket failed\n");
		close (server_socket);
		exit (EXIT_FAILURE);
	}

	if (listen (server_socket, 64) < 0) {
		fprintf (stderr, "==SERVER ERROR: Listening failed\n");
		close (server_socket);
		exit (EXIT_FAILURE);
	}

	info ("==SERVER: Ready to accept on %s:%d.\n", inet_ntoa (address.sin_addr), prg_opts.port);

	sockaddr_in client_addr;						// client socket data structure
	socklen_t client_len = sizeof (client_addr);
	
	pthread_t thr;
	pthread_arg_struct parameters;
	while (true) {
		fd_set filedesc_set;
		FD_ZERO (&filedesc_set);
		FD_SET (STDIN_FILENO, &filedesc_set);
		FD_SET (server_socket, &filedesc_set);

		int ret = select (server_socket + 1, &filedesc_set, 0, 0, 0);
		if (ret <= 0) {								// select error
			fprintf (stderr, "==SERVER ERROR: Select failed\n");
			close (server_socket);
			break;
		} 
		
		if (FD_ISSET (STDIN_FILENO, &filedesc_set)) {	// something on stdin
			char ch[10] = { 0 };
			read (STDIN_FILENO, ch, 10);
			info ("SERVER: STDIN input: %s\n", ch);
			if (ch[0] == 'q') {
				info ("SERVER: STDIN quit\n", ch);
				break;
			}
		}
		if (FD_ISSET (server_socket, &filedesc_set)) {	// something to accept
			int client_socket = accept (server_socket, (sockaddr *) &client_addr, &client_len);
			if (client_socket < 0) {
				fprintf (stderr, "==SERVER ERROR: Accepting connection failed\n");
				close (server_socket);
				close (client_socket);
				exit (EXIT_FAILURE);
			}

			parameters = { &table, client_socket };
			fprintf (stderr, "==SERVER: Connection Socket:%d\n", client_socket);

			sem_wait (&thr_mut_in);
			int val = pthread_create (&thr, NULL, child_process, (void *) &parameters);
			if (val != 0) {						// thread error
				fprintf (stderr, "==SERVER ERROR: Creating a thread failed\n");
				close (server_socket);
				close (client_socket);
				exit (EXIT_FAILURE);
			}
			pthread_detach (thr);
			sem_wait (&thr_mut_out);			// wait for signal from child
		}
	}

	return 0;
}

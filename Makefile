filename-f = filo-fork
filename-t = filo-thr
filename-all = filo-fork filo-thr
objects-f = filosofove_server_processes.o dining_table.o shared_mem.o utils.o
objects-t = filosofove_server_threads.o dining_table.o shared_mem.o utils.o
objects-all = filosofove_server_processes.o filosofove_server_threads.o dining_table.o shared_mem.o utils.o

CPPFLAGS = -g -pthread -std=c++14 -Wall -pedantic
LDFLAGS = -g -pthread -lrt

# build both files by default
all : filo-fork filo-thr
.PHONY : all

# server implemented using forks
filo-fork : $(objects-f)
	g++ -o $(filename-f) $(objects-f) $(LDFLAGS)

#server implemented using threads
filo-thr : $(objects-t)
	g++ -o $(filename-t) $(objects-t) $(LDFLAGS)

filosofove_server_processes.o : filosofove_server_processes.cpp file_descriptor_reader.hpp dining_table.h shared_mem.h utils.h
	g++ -c $(CPPFLAGS) filosofove_server_processes.cpp

filosofove_server_threads.o : filosofove_server_threads.cpp file_descriptor_reader.hpp dining_table.h shared_mem.h utils.h
	g++ -c $(CPPFLAGS) filosofove_server_threads.cpp

dining_table.o : dining_table.cpp dining_table.h
	g++ -c $(CPPFLAGS) dining_table.cpp

shared_mem.o : shared_mem.cpp shared_mem.h
	g++ -c $(CPPFLAGS) shared_mem.cpp
	
utils.o : utils.cpp utils.h
	g++ -c $(CPPFLAGS) utils.cpp

.PHONY : clean

clean :
	-rm $(objects-all) $(filename-all)
